<?php


// Pull custom styles from styles.css file.
add_action(
  'wp_enqueue_scripts',
  function () {
    wp_enqueue_style( 'breakroom-style', get_stylesheet_directory_uri() . '/style.css' );
  }
);

// Helly R ... Please approach the MDE cart!
add_action(
  'wp_enqueue_scripts',
  function () {
    if ( isset( $_GET[ 'breakroom' ] ) ) {
      wp_enqueue_script( 'milchick', get_stylesheet_directory_uri() . '/js/milchick.js' , null, null, true );
    }
  }
);


