
// (I realize we would never do it this way.)
document.querySelector( 'main#content' ).insertAdjacentHTML( 'afterbegin',
`<section class="miz-layer miz-layer--brand miz-fill--black-100 miz-decoration miz-decoration__plus miz-decoration__plus--bottom-right miz-main-grid__full">
  <div class="miz-container">
    <div class="miz-decoration miz-decoration__corner--top-left">
      <div class="miz-big-message__frame miz-fill--white">
        <header class="miz-layer__header miz-layer__header--center">
          <h2 class="miz-layer__title">Break room</h2>
        </header>
        <div id="break-room" class="miz-layer__content miz-layer__content--center miz-big-message__content">
          <p><button id="btn" class="miz-button miz-button--icon miz-button--primary miz-button--small miz-button--square-sm miz-input-group__button">All I can be is sorry, and that is all I am.</button></p>
          <p id="milchick">Read it.</p>
        </div>
      </div>
    </div>
  </div>
</section>`);

let btn      = document.querySelector( '#btn'      );
let milchick = document.querySelector( '#milchick' );

btn.addEventListener( 'click', () => {
  btn.setAttribute( 'disabled', 'disabled' );

  milchick.innerText = '(Evaluating ...)';
  milchick.classList.add( 'evaluating' );

  setTimeout( () => {
    milchick.innerText = "I'm afraid you don't mean it. Again, please.";
    milchick.classList.remove( 'evaluating' );

    btn.removeAttribute( 'disabled' );
  }, 3000);
});

